package com.sashashtmv.callmanager.adapters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sashashtmv.callmanager.R;
import com.sashashtmv.callmanager.fragments.OrderFragment;

import com.sashashtmv.callmanager.model.ModelOrder;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.ContentValues.TAG;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {

    private List<ModelOrder> mModelOrders;
    protected static ItemListener mListener;

    protected ModelOrder item;
    OrderFragment mOrderFragment;
    Context mContext;

    public OrderAdapter(List<ModelOrder> modelOrders, ItemListener listener, Context context) {
        mModelOrders = modelOrders;
        mListener = listener;
        mContext = context;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new OrderViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_order, null));
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.OrderViewHolder viewHolder, int position) {

        item = mModelOrders.get(position);
//        Diagnostics.i(this, "OrderAdapter_onBindViewHolder success" ).append("LOG_FILE");
        Log.i(TAG, "onBindViewHolder: position - " + position);
        viewHolder.descriptionOrder.setText(item.getNameOfBuyer() + "/Id" + item.getId());
    }

    @Override
    public int getItemCount() {
        return mModelOrders.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView baseImage;

        protected TextView descriptionOrder;

        public OrderViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            baseImage = itemView.findViewById(R.id.iv_base_image);
            descriptionOrder = itemView.findViewById(R.id.tv_description_order);
            baseImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    (new AsyncTask<Void, Void, Void>() {
//                        boolean starting = false;
//                        @Override
//                        protected Void doInBackground(Void... params) {
//                            Intent intent = new Intent(Intent.ACTION_CALL);
//                            intent.setData(Uri.parse("tel:" + mModelOrders.get(getAdapterPosition()).getTelephoneOfBuyer()));
//                            Log.i(TAG, "onClick: success");
//                            mContext.startActivity(intent);
//                            return null;
//                        }
//
//                        @Override
//                        protected void onPostExecute(Void aVoid) {
//                            if(starting) {
////                                Intent myApp = getPackageManager().getLaunchIntentForPackage("com.my.myapp");
////                                if (myApp != null) {
////                                    startActivity(myApp);
////                                }
//                            }
//                        }
//                    }).execute();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + mModelOrders.get(getAdapterPosition()).getTelephoneOfBuyer()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    Diagnostics.i(this, "OrderAdapter_onClick success" ).append("LOG_FILE");
                    Log.i(TAG, "onClick: success");
                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    mContext.startActivity(intent);
//                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//
//
//                    }
                }
            });
        }

        @Override
        public void onClick(View v) {
            if (item != null && OrderAdapter.mListener != null) {
                OrderAdapter.mListener.onItemClick(item);
//                Diagnostics.i(this, "OrderAdapter_onClick success1" + item.getNameOfBuyer()).append("LOG_FILE");
                Log.i(TAG, "onClick: item -" + item.getNameOfBuyer());
            }
        }
    }

    public interface ItemListener {
        void onItemClick(ModelOrder item);
    }

    public void removeAllItems() {
        if (getItemCount() != 0) {
            mModelOrders = new ArrayList<>();
//            Diagnostics.i(this, "OrderAdapter_removeAllItems success" ).append("LOG_FILE");
            notifyDataSetChanged();

        }
    }

    public void addOrders(List<ModelOrder> list) {
        mModelOrders = list;
//        Diagnostics.i(this, "OrderAdapter_addOrdres success" ).append("LOG_FILE");
        notifyDataSetChanged();
    }
}
