package com.sashashtmv.callmanager.adapters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sashashtmv.callmanager.R;
import com.sashashtmv.callmanager.model.ModelOrder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.ContentValues.TAG;

public class CallLogAdapter extends RecyclerView.Adapter<CallLogAdapter.CallLogViewHolder> {
    private List<ModelOrder> mModelOrders;
    protected ModelOrder item;
    private Context mContext;
    protected static ItemListener mListener;

    public CallLogAdapter(Context context, List<ModelOrder> list) {
        mContext = context;
        mModelOrders = list;
    }


    @NonNull
    @Override
    public CallLogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CallLogViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order, null));
    }

    @Override
    public void onBindViewHolder(@NonNull CallLogViewHolder holder, int position) {
        item = mModelOrders.get(position);
//        Diagnostics.i(this, "OrderAdapter_onBindViewHolder success" ).append("LOG_FILE");
        Log.i(TAG, "onBindViewHolder: position - " + position);
        holder.descriptionOrder.setText(item.getNameOfBuyer() + "/Id" + item.getId());
        holder.time.setVisibility(View.VISIBLE);
//        Date date = new Date(item.getTime());
        holder.time.setText( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(item.getTime())) + "/"
                +  new SimpleDateFormat("mm:ss").format(new Date(item.getCallDuration())));


    }

    @Override
    public int getItemCount() {
        return mModelOrders.size();
    }

    public class CallLogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected ImageView baseImage;

        protected TextView descriptionOrder;
        protected TextView time;


        public CallLogViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            baseImage = itemView.findViewById(R.id.iv_base_image);
            descriptionOrder = itemView.findViewById(R.id.tv_description_order);
            time = itemView.findViewById(R.id.tv_time);
            baseImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + mModelOrders.get(getAdapterPosition()).getTelephoneOfBuyer()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    Diagnostics.i(this, "OrderAdapter_onClick success" ).append("LOG_FILE");
                    Log.i(TAG, "onClick: success");
                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    mContext.startActivity(intent);
//                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//
//
//                    }
                }
            });
        }

        @Override
        public void onClick(View v) {
            if (item != null && OrderAdapter.mListener != null) {
                CallLogAdapter.mListener.onItemClick(item);
//                Diagnostics.i(this, "OrderAdapter_onClick success1" + item.getNameOfBuyer()).append("LOG_FILE");
                Log.i(TAG, "onClick: item -" + item.getNameOfBuyer());
            }
        }
    }

    public interface ItemListener {
        void onItemClick(ModelOrder item);
    }

    public void removeAllItems() {
        if (getItemCount() != 0) {
            mModelOrders = new ArrayList<>();
//            Diagnostics.i(this, "OrderAdapter_removeAllItems success" ).append("LOG_FILE");
            notifyDataSetChanged();

        }
    }

    public void addOrders(List<ModelOrder> list) {
        mModelOrders = list;
//        Diagnostics.i(this, "OrderAdapter_addOrdres success" ).append("LOG_FILE");
        notifyDataSetChanged();
    }
}
