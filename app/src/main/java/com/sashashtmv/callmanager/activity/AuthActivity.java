package com.sashashtmv.callmanager.activity;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sashashtmv.callmanager.R;
import com.sashashtmv.callmanager.api.APIClient;
import com.sashashtmv.callmanager.api.APIInterface;
import com.sashashtmv.callmanager.api.RequestBody;
import com.sashashtmv.callmanager.api.ResponseData;

import com.sashashtmv.callmanager.model.PreferenceHelper;
import com.sashashtmv.callmanager.receivers.BlockOutgoing;
import com.sashashtmv.callmanager.receivers.DeviceAdminDemo;
import com.sashashtmv.callmanager.receivers.TService;

import org.acra.ACRA;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class AuthActivity extends AppCompatActivity {

    private EditText mLogin;
    private EditText mPassword;
    private Button mEnter;
    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;

    private static final int PERMISSION_REQUEST_READ_PHONE_STATE = 1;
    private static final int REQUEST_CODE = 0;
    private DevicePolicyManager mDPM;
    private ComponentName mAdminName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_auth);

        mLogin = findViewById(R.id.etLogin);
        mPassword = findViewById(R.id.etPassword);
        mEnter = findViewById(R.id.buttonEnter);
        apiInterface = APIClient.getClient().create(APIInterface.class);

//        new Diagnostics(this);
//        Diagnostics.createLog("LOG_FILE", this);

        try {
            mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
            mAdminName = new ComponentName(this, DeviceAdminDemo.class);
            Log.i(TAG, "onCreate: mPDM - " + mDPM.isAdminActive(mAdminName));
//            Diagnostics.i(this, "onCreate mPDM0").append("LOG_FILE");
            if (!mDPM.isAdminActive(mAdminName)) {
                Log.i(TAG, "onCreate: access1");
                Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminName);
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Click on Activate button to secure your application.");
                startActivityForResult(intent, REQUEST_CODE);
//                Diagnostics.i(this, "onCreate completed mPDM").append("LOG_FILE");
            } else {
                Log.i(TAG, "onCreate: access2");
                Intent intent = new Intent(AuthActivity.this, TService.class);
                startService(intent);
//                Diagnostics.i(this, "onCreate start TService").append("LOG_FILE");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        ComponentName component = new ComponentName(this, BlockOutgoing.class);
//        int status = this.getPackageManager().getComponentEnabledSetting(component);
//        if(status == PackageManager.COMPONENT_ENABLED_STATE_ENABLED) {
//            Log.i("receiver","receiver is enabled");
//        } else if(status == PackageManager.COMPONENT_ENABLED_STATE_DISABLED) {
//            Log.i("receiver","receiver is disabled");
//        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            String[] permissions = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE};
            requestPermissions(permissions, PERMISSION_REQUEST_READ_PHONE_STATE);
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE};
                requestPermissions(permissions, PERMISSION_REQUEST_READ_PHONE_STATE);
            }
        }
        PreferenceHelper.getInstance().init(getApplicationContext());
        mPreferenceHelper = PreferenceHelper.getInstance();
        if(mPreferenceHelper.getString("access_token").length() > 0){
//            Intent intent = new Intent(AuthActivity.this, TService.class);
//            startService(intent);
            startActivity(new Intent(AuthActivity.this, MainActivity.class));
//            Diagnostics.i(this, "onCreate autoEnter in app").append("LOG_FILE");
        }else {
//            Intent intent = new Intent(AuthActivity.this, TService.class);
//            startService(intent);
        }

        mEnter.setOnClickListener(mOnEnterClickListener);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult: access");
        if (REQUEST_CODE == requestCode) {
//            Diagnostics.i(this, "onActivityResult completed").append("LOG_FILE");
            Intent intent = new Intent(AuthActivity.this, TService.class);
            startService(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_READ_PHONE_STATE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Diagnostics.i(this, "onRequestPermissionsResult0 completed").append("LOG_FILE");
//                    Toast.makeText(this, "Permission granted: " + PERMISSION_REQUEST_READ_PHONE_STATE, Toast.LENGTH_SHORT).show();
                    Log.i(TAG, "onRequestPermissionsResult: completed0");
                } else {
                    Log.i(TAG, "onRequestPermissionsResult: completed1");
//                    Diagnostics.i(this, "onRequestPermissionsResult1 completed").append("LOG_FILE");
//                    Toast.makeText(this, "Permission NOT granted: " + PERMISSION_REQUEST_READ_PHONE_STATE, Toast.LENGTH_SHORT).show();
                }

                return;
            }
        }
    }

    private View.OnClickListener mOnEnterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (checkValidation()) {
                if (CommonMethod.isNetworkAvailable(AuthActivity.this)) {
//                    Diagnostics.i(this, "onEnterClickListener completed").append("LOG_FILE");
                    loginRetrofit2Api(mLogin.getText().toString(), mPassword.getText().toString());
                }
                else
                    CommonMethod.showAlert("Internet Connectivity Failure", AuthActivity.this);
            }
//            showMessage(R.string.login_input_error);
        }
    };

    private void showMessage(@StringRes int string) {
        Toast.makeText(this, string, Toast.LENGTH_LONG).show();
    }

    private void loginRetrofit2Api(String userId, String password) {
        Log.i("TAG", "loginRetrofit2Api: - " + userId  + " " + password);
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("user_name", userId);
            paramObject.put("password", password);
            Call<Object> call1 = apiInterface.createUser(paramObject.toString());
//            Diagnostics.i(this, "LoginRetrofit2AuthActivity completed").append("LOG_FILE");
            call1.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse1: " + response.code() + response.body());
//                    Diagnostics.i(this, "LoginRetrofit2onResponseAuthActivity completed" + response.code() + response.body()).append("LOG_FILE");
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                            boolean access = object.getBoolean("success");
                            String token = object.getString("access_token");
                            if(access){
                                mPreferenceHelper.putString("access_token", token);
//                                Diagnostics.i(this, "LoginRetrofit2onResponseAuthActivity completed" + access + token).append("LOG_FILE");
                                startActivity(new Intent(AuthActivity.this, MainActivity.class));
                            }
                            Log.e("TAG", "response 33: " + access + token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
//                    Diagnostics.i(this, "LoginRetrofit2onResponseAuthActivity failure").append("LOG_FILE");
//                    Toast.makeText(getApplicationContext(), "onFailure called ", Toast.LENGTH_SHORT).show();
                    call.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkValidation() {

        if (mLogin.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("UserId Cannot be left blank", this);
            return false;
        } else if (mPassword.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("password Cannot be left blank", this);
            return false;
        }
        return true;
    }

    public static class CommonMethod {


//        public static final String DISPLAY_MESSAGE_ACTION =
//                "com.codecube.broking.gcm";
//
//        public static final String EXTRA_MESSAGE = "message";

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            try {
                builder.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Diagnostics.i(this, "onDestroy in AuthActivity completed" ).append("LOG_FILE");
        //getApplicationContext().startActivity(getIntent());
        //registerReceiver(new IncomingCallReceiver(), new IntentFilter("android.intent.action.PHONE_STATE"));
    }
}
