package com.sashashtmv.callmanager.activity;

import android.Manifest;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.app.admin.DevicePolicyManager;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sashashtmv.callmanager.MyApp;
import com.sashashtmv.callmanager.R;
import com.sashashtmv.callmanager.adapters.CallLogAdapter;
import com.sashashtmv.callmanager.adapters.OrderAdapter;
import com.sashashtmv.callmanager.api.APIClient;
import com.sashashtmv.callmanager.api.APIInterface;
import com.sashashtmv.callmanager.fragments.CallLogFragment;
import com.sashashtmv.callmanager.fragments.OrderFragment;

import com.sashashtmv.callmanager.logs.FeedbackEmail;
import com.sashashtmv.callmanager.model.AppDatabase;
import com.sashashtmv.callmanager.model.ModelOrder;
import com.sashashtmv.callmanager.model.OrderDao;
import com.sashashtmv.callmanager.model.PreferenceHelper;
import com.sashashtmv.callmanager.receivers.BlockIngoing;
import com.sashashtmv.callmanager.receivers.BlockOutgoing;
import com.sashashtmv.callmanager.receivers.DeviceAdminDemo;
import com.sashashtmv.callmanager.receivers.TService;
import com.sashashtmv.callmanager.updater.SettingsManager;
import com.sashashtmv.callmanager.updater.Updater;

import org.acra.ACRA;
import org.acra.ErrorReporter;
import org.acra.log.ACRALog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Header;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity implements OrderFragment.OnFragmentInteractionListener {

    private APIInterface apiInterface;
    public static FragmentManager mFragmentManager;

//    public static String temp;

    private PreferenceHelper mPreferenceHelper;
    private OrderAdapter mAdapter;
    private CallLogAdapter mCallLogAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        new Updater().execute(this);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        mFragmentManager = getFragmentManager();
        PreferenceHelper.getInstance().init(getApplicationContext());
        mPreferenceHelper = PreferenceHelper.getInstance();
        List<ModelOrder> list = new ArrayList<>();
        list.add(new ModelOrder(1, "Natasha", "963681344"));

        mAdapter = new OrderAdapter(list, null, this);

        if (mPreferenceHelper != null && mPreferenceHelper.getString("access_token") != null) {
//            Diagnostics.i(this, "onCreateMainActivity completed0").append("LOG_FILE");
            new LoadDataTask().execute(mPreferenceHelper.getString("access_token"));

        } else {
//            Diagnostics.i(this, "onCreateMainActivity completed1").append("LOG_FILE");
            startActivity(new Intent(MainActivity.this, AuthActivity.class));
        }
    }

    private void runOrderFragment(List<ModelOrder> list) {
        OrderFragment orderFragment = OrderFragment.newInstance(mAdapter, list);
//        Diagnostics.i(this, "runOrderFragment completed").append("LOG_FILE");
        mFragmentManager.beginTransaction()
                .replace(R.id.container, orderFragment, "order fragment")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCallLogFragment() {

        AppDatabase db = MyApp.getInstance().getDatabase();
        OrderDao orderDao = db.orderDao();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                List<ModelOrder> test = orderDao.getAll();
                for (int i = 0; i < test.size(); i++) {
                    if((test.get(i).getTime() + 86400000) < new Date().getTime()){
                        orderDao.delete(test.get(i));
                    }
                }
                test = orderDao.getAll();
        CallLogFragment callLogFragment = CallLogFragment.newInstance(test);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, callLogFragment, "callLogFragment fragment")
                .addToBackStack(null)
                .commit();
                for (int j = 0; j < test.size(); j++) {

                    Log.i(TAG, "onReceive: number - " + "phone - " + test.get(j).getTelephoneOfBuyer() + " id - " + test.get(j).getId() +
                            " time - " + test.get(j).getTime() + " name - " + test.get(j).getNameOfBuyer());

                }

            }
        });

    }

    @Override
    public void updateTelephoneList() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
//                Diagnostics.i(this, "updateTelephoneList completed").append("LOG_FILE");
                processData();
            Log.i(TAG, "updateData: load data");
            }
        });
    }

    private class LoadDataTask extends AsyncTask<String, String, String> {
        ProgressDialog pd = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute: load data");
            super.onPreExecute();
//            Diagnostics.i(this, "LoadDataTask_onPreExecute completed").append("LOG_FILE");
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.show();
            Log.i(TAG, "onPreExecute: success");

        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i(TAG, "doInBackground: token1 - " + strings[0]);


            return getData();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            new Updater().execute(MainActivity.this);
            Log.i(TAG, "onPostExecute: success");
//            temp = s;
            java.util.TimerTask task = new java.util.TimerTask() {
                @Override
                public void run() {
//                    Diagnostics.i(this, "LoadDataTask_onPostExecuteTimerTask completed").append("LOG_FILE");
                    processData();
                }
            };
            java.util.Timer timer = new java.util.Timer(true);// true to run timer as daemon thread
            timer.schedule(task, 0, 60000);// Run task every 5 second
            Log.i("TAG", "open order fragment: " + "success");

        }

    }

    protected String getData() {
        String token = mPreferenceHelper.getString("access_token");
        String data;
        Call<Object> call1 = apiInterface.createOrders("Bearer " + token);

        Log.i(TAG, "doInBackground: success" + token + call1.toString());
//        Diagnostics.i(this, "MainActivity_getData completed" + token + call1.toString()).append("LOG_FILE");
        JSONArray array = null;

        try {
            Response response = call1.execute();
            Log.i(TAG, "onResponse: success" + response.code());
//            Diagnostics.i(this, "MainActivity_getData1 completed" +response.code());

            if (response.code() == 401) {
                mPreferenceHelper.putString("access_token", "");
//                Diagnostics.i(this, "MainActivity_getData2 completed" +response.code());
                startActivity(new Intent(MainActivity.this, AuthActivity.class));
            } else {
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        array = new JSONArray(new Gson().toJson(response.body()));
                        data = array.toString();
                        if(array.length() == 0){
                            data = "";
                        }
//                        Diagnostics.i(this, "MainActivity_getData3 completed" + response.body()
//                                + array.toString() + response.isSuccessful() + mPreferenceHelper.getString("access_token"));
                        Log.i(TAG, "onResponse: success - " + response.body() + array.toString() + response.isSuccessful() + mPreferenceHelper.getString("access_token"));
                        Log.i(TAG, "getData: data - " + data);
                        return data;

                        //Log.e("TAG", "response 33: " + object.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    protected void processData() {
        try {
            JSONArray array = new JSONArray(getData());
            List<String> listNumber = new ArrayList<>();
            List<ModelOrder> mList = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                String temp = array.get(i).toString();
                JSONObject jsonObject = new JSONObject(temp);
                String number = jsonObject.getString("phone");
                String country = jsonObject.getString("country");
            Log.i(TAG, "run: success11 - " + "country - " + country + " number - " + number);
//                if(number.length() > 9)
//                String data = number.substring(number.length() - 9) + jsonObject.getString("name") +
//                        "/" + jsonObject.getInt("crm_id") + "/" + jsonObject.getInt("id");
                if (number.length() > 9) {
                    number = number.substring(number.length() - 9);
//                    listNumber.add( number+ jsonObject.getString("name") +
//                        "/" + jsonObject.getInt("crm_id") + "/" + jsonObject.getInt("id"));
                }
                if(country.equals("kg")){
                    number = 0 + number;
                }
//                else number = 0 + number;
                listNumber.add(0,number + jsonObject.getString("name") + "/" + jsonObject.getInt("crm_id") + "/" + jsonObject.getInt("id"));
                Log.i(TAG, "processData: - " + number + jsonObject.getString("name") + "/" + jsonObject.getInt("crm_id") + "/" + jsonObject.getInt("id"));
                ModelOrder modelOrder = new ModelOrder(jsonObject.getInt("crm_id"), jsonObject.getString("name"), number);
//                modelOrder.setCountry(country);
                mList.add(0, modelOrder);
            }
            Log.i(TAG, "run: success - " + mList.toString());
//            Diagnostics.i(this, "MainActivity_processData completed" + mList.toString());
            if(listNumber.isEmpty()) {
//                Diagnostics.i(this, "MainActivity_processData listEmpty" );
                listNumber.add("963681344" + "Natasha/" + 1);
            }
            mPreferenceHelper.putList("numberList", listNumber);
            final List<ModelOrder> list = mList;
            if (list != null && list.size() > 0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (mFragmentManager.findFragmentByTag("order fragment") == null) {
                            runOrderFragment(list);
                        }
                        mAdapter.addOrders(list);
                    }
                });
            }
//            Diagnostics.i(this, "MainActivity_processData completed" + array.toString());
            Log.i("TAG", "getOrders: " + array.toString());
        } catch (JSONException e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (mFragmentManager.findFragmentByTag("order fragment") == null) {
                        runOrderFragment(new ArrayList<>());
                    }
                    mAdapter.addOrders(new ArrayList<>());
                }
            });
            Log.i(TAG, "processData: error get list - true");
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        mPreferenceHelper = PreferenceHelper.getInstance();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Diagnostics.i(this, "onDestroy in MainActivity completed" ).append("LOG_FILE");
//        new FeedbackEmail(this)
//                .setEmail("sashashtmv06@gmail.com")
//                .setSubject("Feedback")
//                .cacheAttach("LOG_FILE")
//                .cacheAttach("compilation")
//                .build()
//                .send();
    }

    public void Update(final Integer lastAppVersion) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Доступно обновление приложения Call Manager до версии " +
                        lastAppVersion + " - желаете обновиться? " +
                        "Если вы согласны - вы будете перенаправлены к скачиванию APK файла,"
                        +" который затем нужно будет открыть.")
                        .setCancelable(true)
                        .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                String apkUrl = "https://github.com/sashashtmv/apk/blob/master/app-debug.apk";
//                                String apkUrl = "https://github.com/jehy/rutracker-free/releases/download/" + lastAppVersion + "/app-release.apk";
                                //intent.setDataAndType(Uri.parse(apkUrl), "application/vnd.android.package-archive");
                                intent.setData(Uri.parse(apkUrl));

                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SettingsManager.put(MainActivity.this, "LastIgnoredUpdateVersion", lastAppVersion.toString());
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    public APIInterface getApiInterface() {
        return apiInterface;
    }

    public PreferenceHelper getPreferenceHelper() {
        return mPreferenceHelper;
    }

}
