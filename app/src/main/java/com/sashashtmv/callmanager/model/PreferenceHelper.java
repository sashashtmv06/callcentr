package com.sashashtmv.callmanager.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sashashtmv.callmanager.activity.MainActivity;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class PreferenceHelper {
    private static PreferenceHelper instance;

    private Context mContext;

    private SharedPreferences mSharedPreferences;

    public PreferenceHelper() {
    }

    public static PreferenceHelper getInstance() {
        if (instance == null) {
            instance = new PreferenceHelper();
        }
        return instance;
    }

    public void init(Context context) {
        this.mContext = context;
        this.mSharedPreferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
    }

    public void putString(String key, String value) {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getString(String key) {
        return mSharedPreferences.getString(key, "");
    }

    public void putList(String key, List<String> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, json);
        editor.apply();
    }

    public List<String> getList(String key) {
        Gson gson = new Gson();
        String json = mSharedPreferences.getString(key, "");
        List<String> arrPackageData = new ArrayList<>();
        if (!json.isEmpty()) {

            Type type = new TypeToken<List<String>>() {
            }.getType();
             arrPackageData = gson.fromJson(json, type);
        }
            return arrPackageData;
    }
}
