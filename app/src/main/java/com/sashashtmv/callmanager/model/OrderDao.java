package com.sashashtmv.callmanager.model;


import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface OrderDao {
    @Query("SELECT * FROM modelorder ORDER BY time DESC")
    List<ModelOrder> getAll();

    @Query("SELECT * FROM modelorder WHERE time = :id")
    ModelOrder getByTime(long id);

    @Query("SELECT * FROM modelorder WHERE id = :id")
    ModelOrder getById(String id);

    @Insert
    void insert(ModelOrder employee);

    @Update
    void update(ModelOrder employee);

    @Delete
    void delete(ModelOrder employee);
}
