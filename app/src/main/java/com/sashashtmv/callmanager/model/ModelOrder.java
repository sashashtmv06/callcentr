package com.sashashtmv.callmanager.model;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ModelOrder {
    @PrimaryKey
    public long time;
    private int id;
    private String nameOfBuyer;
    private String telephoneOfBuyer;
    private long callDuration;

    public ModelOrder(int id, String nameOfBuyer, String telephoneOfBuyer) {
        this.id = id;
        this.nameOfBuyer = nameOfBuyer;
        this.telephoneOfBuyer = telephoneOfBuyer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameOfBuyer() {
        return nameOfBuyer;
    }

    public void setNameOfBuyer(String nameOfBuyer) {
        this.nameOfBuyer = nameOfBuyer;
    }

    public String getTelephoneOfBuyer() {
        return telephoneOfBuyer;
    }

    public void setTelephoneOfBuyer(String telephoneOfBuyer) {
        this.telephoneOfBuyer = telephoneOfBuyer;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getCallDuration() {
        return callDuration;
    }

    public void setCallDuration(long callDuration) {
        this.callDuration = callDuration;
    }

}
