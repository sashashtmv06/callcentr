package com.sashashtmv.callmanager.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {ModelOrder.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract OrderDao orderDao();

    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(final SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE ModelOrder ADD COLUMN callDuration INTEGER DEFAULT 0 NOT NULL");
        }
    };
}

