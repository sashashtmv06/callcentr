package com.sashashtmv.callmanager.api;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface FileUploadService {
    @Multipart
    @POST("telephony/record")
    Call<Object> upload(
            @Header("Authorization")String token,
//            @Part("description") RequestBody description,
            @Part MultipartBody.Part file,
            @Part("lead_id") RequestBody id
    );

}
