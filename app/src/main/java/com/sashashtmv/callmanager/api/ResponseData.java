package com.sashashtmv.callmanager.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseData {
//    @SerializedName("success_token")
//    @Expose
    private String token;
//    @SerializedName("success")
//    @Expose
    private boolean success;


//    public ResponseData(boolean success, String token) {
//        this.success = success;
//        this.token = token;
//    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
