package com.sashashtmv.callmanager.api;

import com.sashashtmv.callmanager.model.PreferenceHelper;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {
    //PreferenceHelper mPreferenceHelper = PreferenceHelper.getInstance();
    @Headers("Content-Type: application/json")
    @POST("auth")
    Call<Object> createUser(@Body String body);
//    Call<ResponseData> createUser(@Body RequestBody body);
//    Call<ResponseData> createUser(@Field("user_name") String username,
//                                  @Field("password") String password,
//                                  Callback<ResponseData> callback);


    @GET("telephony/get")
    Call<Object> createOrders(@Header("Authorization")String token);

    @GET("app/version")
    Call<Object> getVersion(@Header("Authorization")String token);

    @GET("app/download")
    Call<ResponseBody> getApk(@Header("Authorization")String token);

    @Multipart
    @POST("upload")
    Call<ResponseBody> upload(
            @Part("description") RequestBody description,
            @Part MultipartBody.Part file
    );

}
