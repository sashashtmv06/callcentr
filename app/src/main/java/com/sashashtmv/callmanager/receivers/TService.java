package com.sashashtmv.callmanager.receivers;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaRecorder;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.sashashtmv.callmanager.MyApp;
import com.sashashtmv.callmanager.model.AppDatabase;
import com.sashashtmv.callmanager.model.ModelOrder;
import com.sashashtmv.callmanager.model.OrderDao;
import com.sashashtmv.callmanager.model.PreferenceHelper;

import java.io.File;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import android.os.Environment;
import android.os.Handler;

import com.sashashtmv.callmanager.api.FileUploadService;
import com.sashashtmv.callmanager.api.ServiceGenerator;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class TService extends Service {
    MediaRecorder recorder;
    File audiofile;
    String id;
    String statusCall;
    private boolean recordstarted = false;

    private static final String ACTION_IN = "android.intent.action.PHONE_STATE";
    private static final String ACTION_OUT = "android.intent.action.NEW_OUTGOING_CALL";
    private boolean wasRinging = false;
    private CallBr br_call;
    private static List<String> numberList;
    private static List<ModelOrder> allOrders;
    private PreferenceHelper mPreferenceHelper;
    private Toast mToastToShow;
    private long callDuration;
    private long callStart;
    private long callEnd;


    @Override
    public IBinder onBind(Intent arg0) {
//        Diagnostics.i(this, "TService_onBind completed").append("LOG_FILE");
        return null;
    }

    @Override
    public void onDestroy() {
        Log.d("service", "destroy");
//        Diagnostics.i(this, "TService_onDestroy completed").append("LOG_FILE");
        super.onDestroy();

    }

    @Override
    public void onCreate() {

//        mGlobalData = GlobalData.getInstance();
//        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//
//        if (mTimer == null)
//            mTimer = new Timer();
        Log.e(TAG, "onCreate() success");
//        android.os.Debug.waitForDebugger();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        Diagnostics.i(this, "TService_onStartCommand state completed" + " - intent " + intent.getExtras()).append("LOG_FILE");
        IntentFilter filters = new IntentFilter();
        filters.addAction(ACTION_IN);
        filters.addAction(ACTION_OUT);
        final IntentFilter filter = filters;
        this.br_call = new CallBr();
        this.registerReceiver(this.br_call, filter);
        Log.i(TAG, "onStartCommand: completed - " + "intentExtras - " + intent.getExtras());
//        Diagnostics.i(this, "TService_onStartCommand state completed" + br_call.state).append("LOG_FILE");

        return super.onStartCommand(intent, flags, startId);
//        return Service.START_STICKY;
    }

    public void showToast(Context context, String i) {
        // Set the toast and duration
        int toastDurationInMilliSeconds = 60000;
//            mToastToShow = Toast.makeText(context, "Hello world, I am a toast.", Toast.LENGTH_LONG);
        for (int j = 0; j < 15; j++) {
            mToastToShow = Toast.makeText(context, "Current Ring " + i, Toast.LENGTH_LONG);
            mToastToShow.show();
        }

        // Set the countdown to display the toast
//        CountDownTimer toastCountDown;
//        toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 1000 /*Tick duration*/) {
//            public void onTick(long millisUntilFinished) {
//                mToastToShow.show();
//            }
//
//            public void onFinish() {
//                mToastToShow.cancel();
//            }
//        };
//
//        // Show the toast and starts the countdown
//        toastCountDown.start();
    }

    public void startFileRecord(String fileName) {
        File sampleDir = new File(Environment.getExternalStorageDirectory(), "/TestRecording");
        if (!sampleDir.exists()) {
            sampleDir.mkdirs();
        }
        //audiofile = File.createTempFile(file_name, ".amr", sampleDir);
        audiofile = new File(sampleDir, fileName + ".amr");
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
//        Diagnostics.i(this, "TService_startFileRecord record call completed -" + "path -" + path + "audioFileName -" + audiofile.getName()).append("LOG_FILE");
        Log.i(TAG, "onReceive: startFileRecord0 - " + path);

        recorder = new MediaRecorder();
//            recorder = getStringExtra ("extraInfo");

//                          recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);

        recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(audiofile.getAbsolutePath());
        Log.i(TAG, "onReceive: startFileRecord1 - " + audiofile.getAbsolutePath());
//        Diagnostics.i(this, "TService_startFileRecord record call completed - " + "audioFilePath -" + audiofile.getAbsolutePath()).append("LOG_FILE");
        try {
            recorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "onReceive: startFileRecord2");
        recorder.start();
        callStart = new Date().getTime();
//        Diagnostics.i(this, "TService_startFileRecord record start completed").append("LOG_FILE");
        recordstarted = true;

    }


    private void uploadFile(final File file) {
        // create upload service client
//        Diagnostics.i(this, "TService_uploadFile completed -" + "file -" + file.getName()).append("LOG_FILE");
        Log.i("UploadFile", "loadSuccess - " + " success");
        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);

        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("record_file", file.getName(), requestFile);

        // add another part within the multipart request
        Log.i("UploadFile", "loadSuccess1 - " + " success");
        RequestBody idField =
                RequestBody.create(
                        MultipartBody.FORM, id);
        // finally, execute the request
        Log.i("UploadFile", "loadSuccess2 - " + " success");
        Call<Object> call = service.upload("Bearer " + mPreferenceHelper.getString("access_token"), body, idField);
//        Diagnostics.i(this, "TService_uploadFile completed -" + "request with file sent").append("LOG_FILE");
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call,
                                   Response<Object> response) {
//                Diagnostics.i(this, "TService_uploadFile_onResponse completed -" + "responseCode -" + response.code() + response.body()).append("LOG_FILE");
                Log.i("UploadFile", "loadSuccess3 -loadSuccess3 - " + response.code() + response.body());
                file.delete();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
//                Diagnostics.i(this, "TService_uploadFile_onFalure error").append("LOG_FILE");
                Log.i("Upload error:", t.getMessage());
            }
        });
    }

    public class CallBr extends BroadcastReceiver {
        Bundle bundle;
        String state;
        String inCall, outCall;
        private int lastState = TelephonyManager.CALL_STATE_IDLE;

        @Override
        public void onReceive(Context context, Intent intent) {
//                Diagnostics.i(this, "TService_onReceive state completed - " + " action - " + intent.getAction()).append("LOG_FILE");
            AppDatabase db = MyApp.getInstance().getDatabase();
            OrderDao orderDao = db.orderDao();
//            allOrders = orderDao.getAll();
//            for (int i = 0; i < allOrders.size(); i++) {
//                orderDao.delete(allOrders.get(i));
//            }
            bundle = intent.getExtras();
            state = bundle.getString(TelephonyManager.EXTRA_STATE);
            Log.i(TAG, "onReceive: states - " + "intent: " + intent.getAction() + ", state: " + state + ", bundle: " + bundle);
            if (bundle != null && state != null) {

                inCall = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
//                    Diagnostics.i(this, "TService_onReceive state and inCall completed" + state + inCall).append("LOG_FILE");
                Log.i(TAG, "onReceive: access1 - " + " state - " + state);
//                wasRinging = true;

                PreferenceHelper.getInstance().init(context);
                mPreferenceHelper = PreferenceHelper.getInstance();
                numberList = new ArrayList<>(mPreferenceHelper.getList("numberList"));

                Log.i(TAG, "onReceive: access2");

                if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    wasRinging = true;
                    statusCall = "In";
                    if (recordstarted) {
                        statusCall = "Missed";
                        sendCallToCallLog(orderDao, inCall, 0);
                    }
                }
                if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    if (!wasRinging) {
                        statusCall = "Out";
                    }
                    wasRinging = false;
                    Log.i(TAG, "onReceive: access3 - " + "state - " + state);
                    if (!wasRinging) {
                        Log.i(TAG, "onReceive: state0 - " + state + " - " + TelephonyManager.EXTRA_STATE_RINGING);
//                        sendCallToCallLog(orderDao, inCall);
                        String file_name = "In" + new SimpleDateFormat("dd-MM-yyyy hh-mm-ss").format(new Date());
                        startFileRecord(file_name);
                        Log.i(TAG, "onReceive: access4");
//                            Diagnostics.i(this, "TService_onReceive start record inCall completed" + file_name).append("LOG_FILE");
                    }
                }
                if (state != null && state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
//                        Diagnostics.i(this, "TService_onReceive ExtraStateIdle inCall completed" + state).append("LOG_FILE");
                    Log.i(TAG, "onReceive: access5" + "action in " + bundle.getString(TelephonyManager.EXTRA_STATE));
                    if (wasRinging) {
                        statusCall = "Missed";
                        sendCallToCallLog(orderDao, inCall, 0);
                    }
                    if (recordstarted) {
//                        statusCall = "Out";
                        recorder.stop();
                        callEnd = new Date().getTime();
                        callDuration = callEnd - callStart;
                        Log.i(TAG, "onReceive: access6");
                        if (statusCall.equals("Missed")) {
                            statusCall = "Out";
                        }
                        sendCallToCallLog(orderDao, inCall, callDuration);
                        uploadFile(audiofile);
//                            Diagnostics.i(this, "TService_onReceive stop record inCall completed").append("LOG_FILE");
                        recordstarted = false;
                        wasRinging = false;
                    }

//                        }
                }
            }
        }
    }

    public void sendCallToCallLog(OrderDao orderDao, String inCall, long duration) {
        for (int i = 0; i < numberList.size(); i++) {
            Log.i(TAG, "onReceive: numbers - " + "phone - " + inCall.substring(inCall.length() - 9) + " order - " + numberList.get(i) +
                    " - " + numberList.get(i).substring(9) + " index - " + i + " size list - " + numberList.size());
            if (numberList.get(i).contains(inCall.substring(inCall.length() - 9))) {
                id = numberList.get(i).substring(numberList.get(i).indexOf("/") + 1, numberList.get(i).lastIndexOf("/"));
                showToast(getApplicationContext(), id);
                int n = 9;
                if (numberList.get(i).indexOf("0") == 0 && inCall.length() == 10) {
                    n = 10;
                }
                ModelOrder modelOrder = new ModelOrder(Integer.parseInt(id), statusCall + numberList.get(i).substring(n, numberList.get(i).indexOf("/")), inCall);

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        // Insert Data
                        modelOrder.setTime(new Date().getTime());
                        modelOrder.setCallDuration(duration);
                        orderDao.insert(modelOrder);
                        List<ModelOrder> test = orderDao.getAll();
                        for (int j = 0; j < test.size(); j++) {

                            Log.i(TAG, "onReceive: number - " + "phone - " + test.get(j).getTelephoneOfBuyer() + " id - " + test.get(j).getId() +
                                    " time - " + test.get(j).getTime() + " name - " + test.get(j).getNameOfBuyer());

                        }

                    }
                });
//                            Diagnostics.i(this, "TService_onReceive check inCall completed" + inCall + numberList.get(i)).append("LOG_FILE");
//
                break;
            }
        }
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction();
    }

}
