package com.sashashtmv.callmanager.receivers;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;

import android.graphics.Color;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.BlockedNumberContract;

import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sashashtmv.callmanager.model.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;

import static android.content.ContentValues.TAG;

public class BlockOutgoing extends BroadcastReceiver {

    private static List<String> numberList;
    private boolean isAvailable = false;
    private String number;
    private PreferenceHelper mPreferenceHelper;
    Bundle bundle;
    String state;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onReceive(Context context, Intent intent) {
        bundle = intent.getExtras();
        state = bundle.getString(TelephonyManager.EXTRA_STATE);
        Log.i(TAG, "onReceive: states blockoutgoing- " + "intent: " + intent.getAction() + ", state: " + state + ", bundle: " + bundle);
     PreferenceHelper.getInstance().init(context);
    mPreferenceHelper = PreferenceHelper.getInstance();



        number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        numberList = new ArrayList<>(mPreferenceHelper.getList("numberList"));
        Log.i("12280", "asdasNumber is-->> " + numberList.size());
//        if (getSystemService(TelecomManager.class).getDefaultDialerPackage() != getPackageName()) {
//            Intent ChangeDialer = new Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER);
//        ChangeDialer.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, getPackageName());
//        startActivity(ChangeDialer); }
//        ContentValues values = new ContentValues();
//        values.put(BlockedNumberContract.BlockedNumbers.COLUMN_ORIGINAL_NUMBER, "1234567890");
//        Uri uri = getContentResolver().insert(BlockedNumberContract.BlockedNumbers.CONTENT_URI, values);
        Log.i("12280", "asdasNumber is-->> " + number);
        for (int i = 0; i < numberList.size(); i++) {

            if (numberList.get(i).contains(number.substring(number.length() - 9))) {
        Log.i("12280", "asdasNumber is-->> " + number);
                isAvailable = true;
                break;
            } else {
                isAvailable = false;
            }
        }
        if (!isAvailable) {
            setResultData(null);

        }
//        Toast.makeText(context, "Outgoing Call Blocked", Toast.LENGTH_LONG).show();

    }

    //public static void setNumberList(List<String> list) {
//        numberList = list;
//    }
}
