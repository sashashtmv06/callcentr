package com.sashashtmv.callmanager.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.android.internal.telephony.ITelephony;
import com.sashashtmv.callmanager.activity.MainActivity;
import com.sashashtmv.callmanager.api.FileUploadService;
import com.sashashtmv.callmanager.api.ServiceGenerator;

import com.sashashtmv.callmanager.model.PreferenceHelper;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class BlockIngoing extends BroadcastReceiver {
    private static List<String> numberList;
    private PreferenceHelper mPreferenceHelper;
    private boolean isAvailable = false;
    Bundle bundle;
    String state;
    private String id;
    MediaRecorder recorder;
    File audiofile;
    private Toast mToastToShow;
    private boolean recordstarted = true;


    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        bundle = intent.getExtras();
        state = bundle.getString(TelephonyManager.EXTRA_STATE);
        Log.i(TAG, "onReceive: states blockingoing - " + "intent: " + intent.getAction() + ", state: " + state + ", bundle: " + bundle);

        PreferenceHelper.getInstance().init(context);
        mPreferenceHelper = PreferenceHelper.getInstance();
        numberList = new ArrayList<>(mPreferenceHelper.getList("numberList"));

        if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
            Intent i = new Intent(context, MainActivity.class);
            context.startActivity(i);
        }

        ITelephony telephonyService;
        try {

            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);

            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//            bundle = intent.getExtras();
//            state = bundle.getString(TelephonyManager.EXTRA_STATE);
//            Diagnostics.i(this, "state and bundle completed" + "state - " +state + " bundle - " + bundle).append("LOG_FILE");
            try {
                Method m = tm.getClass().getDeclaredMethod("getITelephony");

                m.setAccessible(true);
                Log.i(TAG, "onReceive: succcess");
                telephonyService = (ITelephony) m.invoke(tm);

                for (int i = 0; i < numberList.size(); i++) {

                    Log.i(TAG, "onReceive: number - " + number + numberList.get(i) + " - " + numberList.get(i).substring(9));
                    if (numberList.get(i).contains(number.substring(number.length() - 9))) {
                        isAvailable = true;
                        id = numberList.get(i).substring(numberList.get(i).lastIndexOf("/") + 1);
//                        Diagnostics.i(this, "TService_onReceive check inCall completed"  + numberList.get(i)).append("LOG_FILE");
//                        showToast(context, i);
//                        Toast.makeText(context, "Ring " + numberList.get(i).substring(9), Toast.LENGTH_LONG).show();
                        break;
                    } else {
                        isAvailable = false;
                    }
                }
                if (!isAvailable && number != null) {
                    telephonyService.silenceRinger();
                    telephonyService.endCall();

                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showToast(Context context, int i) {
        // Set the toast and duration
        int toastDurationInMilliSeconds = 60000;
//            mToastToShow = Toast.makeText(context, "Hello world, I am a toast.", Toast.LENGTH_LONG);
        mToastToShow = Toast.makeText(context, "Ring " + numberList.get(i).substring(9, numberList.get(i).lastIndexOf("/")), Toast.LENGTH_LONG);

        // Set the countdown to display the toast
        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                mToastToShow.show();
            }

            public void onFinish() {
                mToastToShow.cancel();
            }
        };
//
//        // Show the toast and starts the countdown
        mToastToShow.show();
        toastCountDown.start();
    }

    public static void setNumberList(List<String> list) {
        numberList = list;
    }


}
