package com.sashashtmv.callmanager;

import android.app.Application;

import android.content.Context;
import android.util.Log;

import com.sashashtmv.callmanager.model.AppDatabase;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
//import org.acra.annotation.AcraCore;
//import org.acra.annotation.AcraMailSender;
//import org.acra.annotation.AcraNotification;
import org.acra.annotation.ReportsCrashes;

import androidx.room.Room;

import static android.content.ContentValues.TAG;

//@AcraCore(buildConfigClass = BuildConfig.class)
//@AcraMailSender(mailTo = "sashashtmv06@gmail.com")
//
//@AcraNotification(resTitle = R.string.crash_title,
//        resText = R.string.crash_text,
//        resChannelName = R.string.app_name)
@ReportsCrashes(
        mailTo = "sashashtmv06@gmail.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.app_name
)
public class MyApp extends Application {
    private AppDatabase database;
    private static MyApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = Room.databaseBuilder(this, AppDatabase.class, "database")
                .addMigrations(AppDatabase.MIGRATION_1_2)
                .allowMainThreadQueries()
                .build();
//        ACRA.init(this);
    }

    public static MyApp getInstance() {
        return instance;
    }

    public AppDatabase getDatabase() {
        return database;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        ACRA.init(this);
        Log.i(TAG, "attachBaseContext: accesss");
    }
}
