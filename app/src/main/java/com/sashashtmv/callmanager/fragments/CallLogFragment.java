package com.sashashtmv.callmanager.fragments;


import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SearchView;

import com.sashashtmv.callmanager.R;
import com.sashashtmv.callmanager.adapters.CallLogAdapter;
import com.sashashtmv.callmanager.adapters.OrderAdapter;
import com.sashashtmv.callmanager.model.ModelOrder;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CallLogFragment extends Fragment {


    private static CallLogAdapter mAdapter;
    private static List<ModelOrder> mList;
    private SearchView mSearchView;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;

    public CallLogFragment() {
        // Required empty public constructor
    }

    public static CallLogFragment newInstance(List<ModelOrder> list) {
        CallLogFragment fragment = new CallLogFragment();
        mList = list;
        Bundle args = new Bundle();

        //json = s;
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_call_log, container, false);
        // Inflate the layout for this fragment
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (toolbar != null) {
            toolbar.setTitle(R.string.list_orders);
            toolbar.setTitleTextColor(getResources().getColor(R.color.design_default_color_primary_dark));

            activity.setSupportActionBar(toolbar);
        }
        mAdapter = new CallLogAdapter(getActivity(), mList);
        mSearchView = view.findViewById(R.id.search_view);
        //getOrders();
        mRecyclerView = view.findViewById(R.id.rvOrderList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//                int a = 1/0;
                findOrder(newText);

                return false;
            }
        });
        return view;
    }

    public void findOrder(String title) {
        //будем удалять все элементы из списка заказов
        //checkAdapter();
//        Diagnostics.i(this, "OrderFragment_findOrder completed" + mAdapter.toString()).append("LOG_FILE");
        if(mAdapter != null) {
            mAdapter.removeAllItems();
            List<ModelOrder> modelOrders = new ArrayList<>();
            for (int i = 0; i < mList.size(); i++) {
                if (mList.get(i).getNameOfBuyer().contains(title) || (mList.get(i).getId() + "").contains(title)) {
                    modelOrders.add(mList.get(i));
                }
                mAdapter.addOrders(modelOrders);

            }
        }

    }

}
