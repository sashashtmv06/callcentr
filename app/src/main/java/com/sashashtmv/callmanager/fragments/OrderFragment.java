package com.sashashtmv.callmanager.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.os.Bundle;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.sashashtmv.callmanager.R;
import com.sashashtmv.callmanager.activity.AuthActivity;
import com.sashashtmv.callmanager.activity.MainActivity;
import com.sashashtmv.callmanager.adapters.OrderAdapter;
import com.sashashtmv.callmanager.logs.Diagnostics;
import com.sashashtmv.callmanager.logs.FeedbackEmail;
import com.sashashtmv.callmanager.model.ModelOrder;
import com.sashashtmv.callmanager.model.PreferenceHelper;
import com.sashashtmv.callmanager.updater.Updater;

import org.acra.ACRA;
import org.acra.ErrorReporter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.ContentValues.TAG;

public class OrderFragment extends Fragment implements OrderAdapter.ItemListener {

    private OnFragmentInteractionListener mListener;
    private static List<ModelOrder> mList = new ArrayList<>();
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected static OrderAdapter mAdapter;
    SearchView mSearchView;
    ImageView imageView;

    private AccountHeader mAccountHeader = null;
    private Drawer mDrawer = null;
    private static String json;
    private PreferenceHelper mPreferenceHelper;
    //private List<String> listNumber = new ArrayList<>();

    public OrderFragment() {
        // Required empty public constructor
    }

    public static OrderFragment newInstance(OrderAdapter adapter, List<ModelOrder> list) {
        OrderFragment fragment = new OrderFragment();
        mAdapter = adapter;
        mList = list;
        Bundle args = new Bundle();

        //json = s;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_order, container, false);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (toolbar != null) {
            toolbar.setTitle(R.string.list_orders);
            toolbar.setTitleTextColor(getResources().getColor(R.color.design_default_color_primary_dark));

            activity.setSupportActionBar(toolbar);
        }
        mSearchView = view.findViewById(R.id.search_view);
        //getOrders();
        mRecyclerView = view.findViewById(R.id.rvOrderList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        PreferenceHelper.getInstance().init(getActivity().getApplicationContext());
        mPreferenceHelper = PreferenceHelper.getInstance();

        if(mAdapter == null){
            startActivity(new Intent(getActivity(), MainActivity.class));
        }
        //mAdapter = new OrderAdapter( mList, this, getActivity());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);

        imageView = view.findViewById(R.id.iv_update);
        imageView.setColorFilter(getResources().getColor(R.color.black),
                PorterDuff.Mode.MULTIPLY);

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//                int a = 1/0;
                findOrder(newText);

                return false;
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.image_click));
                mListener.updateTelephoneList();
                Log.i(TAG, "onClick: update telephone list");

//                new FeedbackEmail(activity)
//                        .setEmail("sashashtmv06@gmail.com")
//                        .setSubject("Feedback")
//                        .cacheAttach("LOG_FILE")
//                        .cacheAttach("compilation")
//                        .build()
//                        .send();

            }
        });

        //создаем профайл пользователя
        final IProfile iProfile = new ProfileDrawerItem().withIcon(R.drawable.ic_dev);

        //его методы добавляют привязку к активити, устанавливают прозрачность статус бара,
        //добавляют фоновую картинку и слушатель клика  по профилю пользователя
        mAccountHeader = new AccountHeaderBuilder()
                .withActivity(getActivity())
                .withTranslucentStatusBar(true)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(new ProfileDrawerItem().withEmail("Версия - " + mPreferenceHelper.getString("version")).withIcon(R.drawable.ic_dev))
                .withCompactStyle(true)
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(@NotNull View view, @NotNull IProfile<?> iProfile, boolean b) {
//                        ActivityUtilities.getInstance().invokeCustomUrlActivity(mActivity, CustomUrlActivity.class,
//                                getResources().getString(R.string.site), getResources().getString(R.string.site_url),false);
                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(@NotNull View view, @NotNull IProfile<?> iProfile, boolean b) {
                        return false;
                    }
                })

                .build();

        mDrawer = new DrawerBuilder()
                .withActivity(getActivity())
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withAccountHeader(mAccountHeader)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Журнал звонков").withIcon(R.drawable.ic_dev).withIdentifier(10).withSelectable(false),

//                        new SecondaryDrawerItem().withName("YouTube").withIcon(R.drawable.ic_youtube).withIdentifier(20).withSelectable(false),
//                        new SecondaryDrawerItem().withName("Facebook").withIcon(R.drawable.ic_facebook).withIdentifier(21).withSelectable(false),
//                        new SecondaryDrawerItem().withName("Twitter").withIcon(R.drawable.ic_twitter).withIdentifier(22).withSelectable(false),
//                        new SecondaryDrawerItem().withName("Google+").withIcon(R.drawable.ic_google_plus).withIdentifier(23).withSelectable(false),
//
//                        new DividerDrawerItem(),
//                        new SecondaryDrawerItem().withName("Оцените приложение").withIcon(R.drawable.ic_rating).withIdentifier(31).withSelectable(false),
//                        new SecondaryDrawerItem().withName("Поделитесь").withIcon(R.drawable.ic_share).withIdentifier(32).withSelectable(false),
//                        new SecondaryDrawerItem().withName("Соглашения").withIcon(R.drawable.ic_privacy_policy).withIdentifier(33).withSelectable(false),

                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName("Обновить приложение").withIcon(R.drawable.update_app).withIdentifier(30).withSelectedBackgroundAnimated(true).withSelectable(false),
                        new SecondaryDrawerItem().withName("Выход").withIcon(R.drawable.exit).withIdentifier(40).withSelectedBackgroundAnimated(true).withSelectable(false)

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(@Nullable View view, int i, @NotNull IDrawerItem<?> iDrawerItem) {
                        if(iDrawerItem != null){
                            Intent intent = null;
                            if(iDrawerItem.getIdentifier() == 10){
                                mListener.onCallLogFragment();
//                                ActivityUtilities.getInstance().invokeNewActivity(mActivity, AboutDevActivity.class, false);
                            }
//                            else if(iDrawerItem.getIdentifier() == 20){
//                                AppUtilities.youtubeLink(mActivity);
//                            }
//                            else if(iDrawerItem.getIdentifier() == 21){
//                                AppUtilities.facebookLink(mActivity);
//                            }else if(iDrawerItem.getIdentifier() == 22){
//                                AppUtilities.twitterLink(mActivity);
//                            }else if(iDrawerItem.getIdentifier() == 23){
//                                AppUtilities.googlePlusLink(mActivity);
//                            }else if(iDrawerItem.getIdentifier() == 30){
//                                ActivityUtilities.getInstance().invokeNewActivity(mActivity, SettingsActivity.class, false);
//                            }else if(iDrawerItem.getIdentifier() == 31){
//                                AppUtilities.rateThisApp(mActivity);
//                            }else if(iDrawerItem.getIdentifier() == 32){
//                                AppUtilities.shareApp(mActivity);
//                            }
                            else if(iDrawerItem.getIdentifier() == 30){
                                new Updater().execute((MainActivity) getActivity());
                            }
                            else if(iDrawerItem.getIdentifier() == 40){
                                mPreferenceHelper.putString("access_token", "");
                                startActivity(new Intent(getActivity(), AuthActivity.class));
                            }
                        }
                        return false;
                    }
                })
                //сохраняет состояние дровера
                //регулирует возможность принудительного открытия панели навигации при первом запуске приложения
                // определяет отображение дровера при свайпе, и метод билд - завершает создание дровера
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(false)
                .withShowDrawerUntilDraggedOpened(false)
                .build();
        return view;
    }
    public void findOrder(String title) {
        //будем удалять все элементы из списка заказов
        //checkAdapter();
//        Diagnostics.i(this, "OrderFragment_findOrder completed" + mAdapter.toString()).append("LOG_FILE");
        if(mAdapter != null) {
            mAdapter.removeAllItems();
            List<ModelOrder> modelOrders = new ArrayList<>();
            for (int i = 0; i < mList.size(); i++) {
                if (mList.get(i).getNameOfBuyer().contains(title) || (mList.get(i).getId() + "").contains(title)) {
                    modelOrders.add(mList.get(i));
                }
                mAdapter.addOrders(modelOrders);

            }
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
//            Diagnostics.i(this, "OrderFragment_onAttach completed" ).append("LOG_FILE");
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString());
        }
    }

    @Override
    public void onDetach() {
//        new FeedbackEmail(getActivity())
//                .setEmail("sashashtmv06@gmail.com")
//                .setSubject("Feedback")
//                .cacheAttach("LOG_FILE")
//                .cacheAttach("compilation")
//                .build()
//                .send();
        super.onDetach();
//        Diagnostics.i(this, "OrderFragment_onDetach completed" ).append("LOG_FILE");

        mListener = null;

    }

    @Override
    public void onItemClick(ModelOrder item) {

    }

    public interface OnFragmentInteractionListener {

        void onCallLogFragment();
        void updateTelephoneList();
    }

}
