package com.sashashtmv.callmanager.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.sashashtmv.callmanager.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UmbrellaFragment extends Fragment {


    public UmbrellaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_umbrella, container, false);
    }

}
